//
//  Delivery.swift
//  LalamoveDeliveries
//
//  Created by Aayush Maheshwari on 3/6/2017.
//  Copyright © 2017 aayush. All rights reserved.
//

import Foundation

enum ParsingError: Error {
    case missing(String)
}

struct Delivery{
    let description: String
    let imageUrl: String
    let address: String
    let latitude: Double
    let longitude: Double
    
    init(description : String, imageUrl : String,address: String,latitude: Double, longitude: Double) {
        self.description = description
        self.imageUrl = imageUrl
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
    }
}

extension Delivery{
    init?(json:[String: AnyObject]) throws {
        guard let location = json["location"] as? [String: Any],
            let address = location["address"] as? String,
            let latitude = location["lat"] as? Double,
            let longitude = location["lng"] as? Double else { throw ParsingError.missing("location")}
        guard let imageUrl = json["imageUrl"] as? String else { throw ParsingError.missing("imageUrl")}
        guard let description = json["description"] as? String else { throw ParsingError.missing("description")}
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
        self.description = description
        self.imageUrl = imageUrl
    }
}
