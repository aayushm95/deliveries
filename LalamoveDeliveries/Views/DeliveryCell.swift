//
//  DeliveryCell.swift
//  Deliveries
//
//  Created by aayush on 15/09/2018.
//  Copyright © 2018 aayush. All rights reserved.
//

import UIKit
import SDWebImage
class DeliveryCell : UICollectionViewCell{
    override init(frame : CGRect){
        super.init(frame : frame)
        setupViews()
    }
    let descriptionLabel: UILabel = {
        let label = UILabel()
        return label
    }()

    let addressLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    let thumbnailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .blue
        imageView.layer.cornerRadius = 30
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    func setupViews(){
        backgroundColor = .white
        addSubview(thumbnailImageView)
        self.contentView.layer.masksToBounds = true
        addSubview(descriptionLabel)
        addSubview(addressLabel)
        addConstraintsWithFormat(format: "H:|-10-[v0(60)]-10-[v1]-10-|", views: [thumbnailImageView,descriptionLabel])
        addConstraintsWithFormat(format: "H:|-10-[v0(60)]-10-[v1]-10-|", views: [thumbnailImageView,addressLabel])
        addConstraintsWithFormat(format: "V:|-10-[v0]-10-|", views: [thumbnailImageView])
        addConstraintsWithFormat(format: "V:|-8-[v0(23)]-8-[v1(23)]-8-|", views: [descriptionLabel,addressLabel])
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) not implemented")
    }

    func setContent(_ delivery:Delivery){
        self.thumbnailImageView.sd_setImage(with: URL(string:delivery.imageUrl), placeholderImage: UIImage(named:"PlaceHolder"))
        self.descriptionLabel.text = delivery.description
        self.addressLabel.text = delivery.address
    }
    func setEntity(_ deliveryentity: DeliveryEntity)
    {
        DispatchQueue.main.async {
            self.addressLabel.text = deliveryentity.address
            self.descriptionLabel.text = deliveryentity.desc
            if let url = deliveryentity.imageURL {
                //self.photoImageview.loadImagesUsingCachingWithURLString(url,placeHolder : UIImage(named : "placeholder"))
                self.thumbnailImageView.loadImagesUsingCacheWithURLString(url, placeHolder: UIImage(named: "placeholder"))
            }
        }
    }
}
