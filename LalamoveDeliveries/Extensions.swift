//
//  Extensions.swift
//  LalamoveDeliveries
//
//  Created by aayush on 19/09/2018.
//  Copyright © . All rights reserved.
//

import UIKit

let imagecache = NSCache<NSString,UIImage>()
extension UIColor{
    static func rgb(red : CGFloat, green: CGFloat, blue : CGFloat)->UIColor{
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
}
extension UIView{
    func addConstraintsWithFormat(format : String, views: [UIView])
    {
        var viewsDict = [String: UIView]()
        for (index,view) in views.enumerated(){
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDict[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDict))
    }
}

extension UIViewController{
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.clear
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
        ai.startAnimating()
        ai.center = spinnerView.center

        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }

        return spinnerView
    }

    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
}

extension UIImageView
{
    func loadImagesUsingCacheWithURLString(_ URLString : String, placeHolder : UIImage?)
    {
        self.image = nil
        if let cachedImage = imagecache.object(forKey: NSString(string: URLString)){
            self.image = cachedImage
            return
        }
        
        if let url = URL(string: URLString)
        {
            URLSession.shared.dataTask(with: url, completionHandler :
            {(data,response,error) in
             if error != nil
             {
                print("Error loading images from url: \(error)")
                DispatchQueue.main.async {
                    self.image = placeHolder
                }
                return
             }
                DispatchQueue.main.async {
                    if let data = data {
                        if let downloadedImage = UIImage(data : data)
                        {
                            imagecache.setObject(downloadedImage, forKey: NSString(string: URLString))
                            self.image = downloadedImage
                        }
                    }
                }
            }).resume()
        }
    }
}
