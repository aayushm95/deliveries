//
//  ViewController.swift
//  Deliveries
//
//  Created by Aayush Maheshwari on 14/09/2018.
//  Copyright © 2018 aayush. All rights reserved.
//

import UIKit
import Foundation
import StatusProvider
import PureLayout
import CoreData

class DeliveryList: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let DeliveryCellID = "DeliveryCellID"
    var offset = 1
    var limit  = 20
    var deliveriesArray: NSMutableArray! = NSMutableArray()
    var spinner: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationItem.title = "Delivery List"
        navigationController?.navigationBar.isTranslucent = false
        collectionView?.backgroundColor = .black
        self.view.backgroundColor = .white
        do {
            try self.fetchedDeliveryController.performFetch()
            print("COUNT Fetched First : \(self.fetchedDeliveryController.sections?.first?.numberOfObjects ?? 0)")
        } catch let error {
            print("Error : \(error)")
        }
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.isPagingEnabled = true
        collectionView?.register(DeliveryCell.self, forCellWithReuseIdentifier: DeliveryCellID)
        DispatchQueue.main.async {
            self.loadDeliveries()
        }
        
    }

    lazy var fetchedDeliveryController: NSFetchedResultsController<NSFetchRequestResult> = {
        let context  = (UIApplication.shared.delegate as! AppDelegate?)?.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: DeliveryEntity.self))
        // restrict the number of cells to be shown to 20
        //fetchRequest.fetchLimit = 20
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "address", ascending: true)]
        let frc = NSFetchedResultsController(fetchRequest:fetchRequest,managedObjectContext:context!,sectionNameKeyPath : nil, cacheName: nil)
//        do
//        {
//            try frc.performFetch()
//        } catch{
//           fatalError("Failed to fetch entities: \(error)")
//        }
        return frc
    }()
    
//    private func cleardata()
//    {
//        do {
//            let context = (UIApplication.shared.delegate as! AppDelegate?)?.persistentContainer.viewContext
//            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: DeliveryEntity.self))
//            do {
//                let objects = try context?.fetch(fetchRequest) as? [NSManagedObject]
//                _ = objects.map($0.map{context.delete($0)})
//                context?.save()
//            } catch let error {
//                print("Error Deleting : \(error)")
//            }
//        }
//    }
    
    func loadDeliveries(){
        spinner = UIViewController.displaySpinner(onView: collectionView!)
        // Call APIHelper to retrieve deliveries info
        APIHelper.getDeliveries(offset: offset,limit: limit,completionHandler:
            { (success, response) in
                UIViewController.removeSpinner(spinner: self.spinner!) // Hide loading
                if(success){
                    if(!response.isEmpty){
                        do{
                            let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing :DeliveryEntity.self))
                            let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchrequest)
                            let context  = (UIApplication.shared.delegate as! AppDelegate?)?.persistentContainer.viewContext
                            do {
                                try context!.execute(deleteRequest)
                                try context!.save()
                                self.deliveriesArray.removeAllObjects()
                            } catch let error as NSError {
                                log.error(error)
                            }
                            guard let deliveriesJson = try JSONSerialization.jsonObject(with: response.data(using: .utf8)!, options: []) as? [[String: AnyObject]] else { return }
                            for delivery in deliveriesJson{
                                let deliveryModel = try Delivery(json:(delivery))// as? [String:AnyObject])!)
                                
                                self.saveEntitytoCoreData(deliveryModel:deliveryModel)
                                
                                if(deliveryModel != nil){
                                    self.deliveriesArray.add(deliveryModel!)
                                }
                            }
                            self.collectionView?.reloadData()
                        }catch let error{
                            self.spinner = UIViewController.displaySpinner(onView: self.collectionView!)
                            log.error(error)
                        }
                    }
                }else{
                    self.spinner = UIViewController.displaySpinner(onView: self.collectionView!)
                    log.error("fail response:"+response)
                }
        }
        )
    }

    func saveEntitytoCoreData(deliveryModel:Delivery?) {
        let context  = (UIApplication.shared.delegate as! AppDelegate?)?.persistentContainer.viewContext
        if let deliveryEntity = NSEntityDescription.insertNewObject(forEntityName: "DeliveryEntity", into: context!) as? DeliveryEntity {
            deliveryEntity.address = deliveryModel?.address
            deliveryEntity.desc = deliveryModel?.description
            deliveryEntity.latitude = (deliveryModel?.latitude)!
            deliveryEntity.longitude = (deliveryModel?.longitude)!
            deliveryEntity.imageURL = (deliveryModel?.imageUrl)!
            do{
                try context?.save()
            } catch let error{
                print(error)
            }
            //deliveryEntity.mediaURL = mediaDictionary?["m"] as? String
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //let currentDelivery : Delivery = deliveriesArray.object(at: indexPath.row) as! Delivery
        let DeliveryCell = collectionView.dequeueReusableCell(withReuseIdentifier: DeliveryCellID, for: indexPath) as! DeliveryCell
        DeliveryCell.layer.cornerRadius = 20
        if(deliveriesArray.count == fetchedDeliveryController.sections?.first?.numberOfObjects)
        {
            let currentcell = deliveriesArray[indexPath.row] as! Delivery
            DeliveryCell.setContent(currentcell)
        }
        else {
        if let deliveryentity = fetchedDeliveryController.object(at: indexPath) as? DeliveryEntity{
            //print("got the saved object")
            DeliveryCell.setEntity(deliveryentity)
            let currentdelivery = Delivery(description : deliveryentity.desc ?? "", imageUrl : deliveryentity.imageURL ?? "",address: deliveryentity.address ?? "" ,latitude: deliveryentity.latitude, longitude: deliveryentity.longitude)
            if(deliveriesArray.count != fetchedDeliveryController.sections?.first?.numberOfObjects){
                deliveriesArray.add(currentdelivery)
            }
            //currentdelivery.address = deliveryentity.address
            //currentdelivery.description = deliveryentity.desc
            
        }
        }
        //else {DeliveryCell.setContent(currentDelivery)}
        return DeliveryCell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard self.deliveriesArray != nil else {return}
        let delivery = deliveriesArray.object(at: indexPath.row) as! Delivery
        self.navigationController?.pushViewController(DeliveryDetailViewController(delivery), animated: true)
        collectionView.deselectItem(at: indexPath, animated: false)
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        if let count = fetchedDeliveryController.sections?.first?.numberOfObjects{
            return count
        }
        return deliveriesArray.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 40, height: 80)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
