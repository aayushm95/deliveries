#Mobile Developement(iOS)

### Feature
1. Retrieve deliveries information from server
2. List deliveries information
3. Display delivery information in detail & show on map
4. Able to work offline after initial use.

### Dependancies
1. CocoaPods is used for dependency management in the project. 
2. Please open the project with "LalamoveDeliveries.xcworkspace".
3. Please setup the mockAPIMobile on local machine(used localhost)

### Supported iOS version
1. iOS 9 or above

### Supports all iPhones and Landscape Orientation
