//
//  Delivery+CoreDataProperties.swift
//  
//
//  Created by Aayush Maheshwari on 25/09/18.
//
//

import Foundation
import CoreData


extension DeliveryEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DeliveryEntity> {
        return NSFetchRequest<DeliveryEntity>(entityName: "DeliveryEntity")
    }

    @NSManaged public var address: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var desc: String?

}
